#include <iostream>

void PrintNumbers(bool ParityInFunction,int N)
{
    for (int i = 0; i <= N; ++i)
    {
        if (i % 2 == ParityInFunction)
        {
            std::cout << i << "\n";
        }
    }
}

int main()
{
    int Parity = 0;
    int N = 0;

    setlocale(LC_ALL, "Rus");

    std::cout << "������� ������� ��������(N)!\n";
    std::cin >> N;
    std::cout << "\n";

    PrintNumbers(0,N);

    std::cout << "������� 1,���� ������ ������� ��ר���� ����� ��� 0, ���� ������ ������� ר���� �����\n";
    std::cin >> Parity;
    std::cout << "\n";

    PrintNumbers(Parity,N);
}